---------------------------------------------------------------
Example Projects for the Type IV Reflexxes Motion Library
---------------------------------------------------------------


***************************************************************
1. Directory Contents
***************************************************************

 - include: Folder for all header files of the Reflexxes API
 - src: Folder for the source code files of the eight sample
   applications
 - Windows_VS2008Express: Folder for project, solution, and
   binary files


***************************************************************
2. Getting Started
***************************************************************
Open the Visual Studio solution file

Windows_VS2008Express\WindowsVS2008Express_Solution.sln

with your Microsoft Visual Studio 2008 development environment.
You may rebuild the entire solution to check whether all files
compile correctly on your system. If so, you can take a look at
one of the simple sample applications in the sample projects
that you can find in your Visual Studio solution to learn about
the simple and clean Reflexxes API and to use it for your own
applications. In case of problems or issues with this procedure,
please contact us at support@reflexxes.com.


***************************************************************
 3. Documentation
***************************************************************

The entire html documentation can be found in the directory
'Documentation'.


***************************************************************
A. Appendix - Entire Folder Structure
***************************************************************

    * ReflexxesExampleProjects
          o include Folder for all header files of the Reflexxes API
          o src Folder for the source code files of the eight sample applications
                + RMLPositionSampleApplications Source code of 01_RMLPositionSampleApplication.cpp, 02_RMLPositionSampleApplication.cpp, 03_RMLPositionSampleApplication.cpp, and 07_RMLPositionSampleApplication.cpp
                + RMLVelocitySampleApplications Source code of 04_RMLVelocitySampleApplication.cpp, 05_RMLVelocitySampleApplication.cpp, 06_RMLVelocitySampleApplication.cpp, and 08_RMLVelocitySampleApplication.cpp
          o Windows_VS2008Express Folder for project, solution, and binary files
                + debug Folder for binary files (with debug information, 32-bit)
                + release Folder for binary files (without debug information, 32-bit)
                + 01_RMLPositionSampleApplication Visual Studio project file for the sample program 01_RMLPositionSampleApplication.cpp
                      # debug Intermediate build files
                      # release Intermediate build files
                + 02_RMLPositionSampleApplication Visual Studio project file for the sample program 02_RMLPositionSampleApplication.cpp
                      # debug Intermediate build files
                      # release Intermediate build files
                + 03_RMLPositionSampleApplication Visual Studio project file for the sample program 03_RMLPositionSampleApplication.cpp
                      # debug Intermediate build files
                      # release Intermediate build files
                + 04_RMLVelocitySampleApplication Visual Studio project file for the sample program 04_RMLVelocitySampleApplication.cpp
                      # debug Intermediate build files
                      # release Intermediate build files
                + 05_RMLVelocitySampleApplication Visual Studio project file for the sample program 05_RMLVelocitySampleApplication.cpp
                      # debug Intermediate build files
                      # release Intermediate build files
                + 06_RMLVelocitySampleApplication Visual Studio project file for the sample program 06_RMLVelocitySampleApplication.cpp
                      # debug Intermediate build files
                      # release Intermediate build files
                + 07_RMLPositionSampleApplication Visual Studio project file for the sample program 07_RMLPositionSampleApplication.cpp
                      # debug Intermediate build files
                      # release Intermediate build files					  
                + 08_RMLVelocitySampleApplication Visual Studio project file for the sample program 08_RMLVelocitySampleApplication.cpp
                      # debug Intermediate build files
                      # release Intermediate build files					  

---------------------------------------------------------------
Copyright (C) 2013 Reflexxes GmbH